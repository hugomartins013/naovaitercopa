package Test;
import static org.junit.Assert.*;
import Model.Usuario;
import org.junit.Test;

public class TesteUsuario {
	
	@Test
	
	public void testNome(){
		Usuario usuariotest = new Usuario();
		
		usuariotest.setNome("Hugo");
		assertEquals("Hugo", usuariotest.getNome());
		
		
	}
	
	public void testTelefone(){
		Usuario usuariotest = new Usuario();
		
		usuariotest.setTelefone("23456789");
		assertEquals("23456789", usuariotest.getTelefone());
		
		
	}
	
	public void testEmail(){
		Usuario usuariotest = new Usuario();
		
		usuariotest.setEmail("Hugomartins_15@hotmail.com");
		assertEquals("Hugomartins_15@hotmail.com", usuariotest.getEmail());
		
		
	}
	
	public void testSenha(){
		Usuario usuariotest = new Usuario();
		
		usuariotest.setSenha("Massa123@");
		assertEquals("Massa123@", usuariotest.getSenha());
		
		
	}
	
	public void testListaUsuario(){
		Usuario usuarioTest = new Usuario();
		usuarioTest.setNome("Hugo");
		usuarioTest.setEmail("Hugomartins_15@hotmail.com");
		usuarioTest.setTelefone("12345678");
		usuarioTest.setSenha("Massa123@");
		usuarioTest.setListaUsuarios(usuarioTest);
		assertEquals(usuarioTest, usuarioTest.getListaUsuarios());
		
	}

}

