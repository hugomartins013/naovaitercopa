package Test;
import static org.junit.Assert.*;

import java.util.ArrayList;

import Model.Reindivicacao;

import org.junit.Test;

public class TesteReindivicacao {
	
@Test
	
	public void testTitulo(){
		Reindivicacao reindivicacaoTest = new Reindivicacao();
		reindivicacaoTest.setTitulo("Não é só por 20 centavos");
		assertEquals("Não é só por 20 centavos", reindivicacaoTest.getTitulo());
		
		
	}
	
	public void testDescricao(){
		Reindivicacao reindivicacaoTest = new Reindivicacao();
		reindivicacaoTest.setDescricao("Protesto contra o aumento das passagens de ônibus nas linhas de São Paulo.");
		assertEquals("Protesto contra o aumento das passagens de ônibus nas linhas de São Paulo.", reindivicacaoTest.getDescricao());
		
	}
	
	public void testListaReindivicacao(){
		Reindivicacao reindivicacaoTest = new Reindivicacao();
		
		reindivicacaoTest.setTitulo("Não é só por 20 centavos");
		reindivicacaoTest.setDescricao("Protesto contra o aumento das passagens de ônibus nas linhas de São Paulo.");
		
		reindivicacaoTest.setListaReindivicacoes(reindivicacaoTest);
		assertEquals(reindivicacaoTest, reindivicacaoTest.getListaReindivicacoes());
		
	}


}
